const NATS = require('node-nats-streaming');

const stan = NATS.connect('test-cluster', 'test', {
  url: process.env.NATS_URL
});
     

stan.on('connect', async () => {
  console.log(`Sink connected to NATS`);

  const optsAll = stan.subscriptionOptions().setDeliverAllAvailable();

  const new_user_subs = await stan.subscribe('new.user', optsAll);
  const user_ids_subs = {};

  new_user_subs.on('message', async (msg) => {
    console.log(`'new.user': ${msg.getData()}`);

    const uid = Number.parseInt(msg.getData());
    user_ids_subs[uid] = await stan.subscribe(`user.${uid}`, optsAll);
    user_ids_subs[uid].on('message', async (msg) => {
      console.log(`ATENTÇÃO CARALHO, message on 'user.${uid}': ${msg.getData()}`);
    });
  });





  //////////////////// test inputs
  setTimeout(() => {
    stan.publish('new.user', '42', (err, guid) => {
      if (err) console.log("Failed publishing");
      else console.log(`published with guid: ${guid}`);
    });
  }, 5000);

  setTimeout(() => {
    stan.publish('user.42', 'SÓ PRA CASTIGAR', (err, guid) => {
      if (err) console.log("Failed publishing");
      else console.log(`published with guid: ${guid}`);
    });
  }, 10000);
});

console.log(`Sink running`);
